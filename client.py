# Lewis Munyi
# P15/36076/2015

# Bob
import socket


def encrypt(text):
    print("\n\tEncrypting: ", text)

    #Find the integer representation of any unicode characters in the string
    [ord(c) for c in text]
    
    mess=[] # Our message array

    # Find the ASCII value of each character and encrypt it using a key that's known by both Bob and Alice
    for i in text: 
        mess.append(ord(i)*2-20)# key 

    print("\tEncrypted ASCII values: ", mess)

    cypher=[] #Our cypher text array

    for i in mess:
        # Convert the new ascii characters to unicode.
        cypher.append(chr(i))

    # Concantenate all the individual characters to a string
    cyphertext = "".join(cypher)
    print("\tEncrypted text: ", cyphertext)

    message = str.encode(cyphertext) # Stringify the message and send it over the "Public" connection. 
    client.send(message)

def decrypt(response):
    cyphertext = response.decode("utf-8") # convert our message into unicode characters
    print("\tDecrypting: ", cyphertext)
    
    mess = [] # Our message array

    for i in cyphertext:
        # Convert each unicode character to its int value, then to its ASCII value, and 
        # run decrypt it using the secret key and append to to our message array 
        mess.append( chr( int( (ord(i)+20)/2 ) ))
    print ("\tDecrypted ASCII Values", mess)

    # Concantenate all the individual characters to a string and returrn them
    cypher = " ".join(mess)
    return cypher

# The program starts here
if __name__ == "__main__":

    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # create an ipv4 socket object (tcp protocol)

    client.connect(('0.0.0.0', 3002)) # connect the client(localhost, port)

    # get message from bob
    text = input('|----Message to send to Alice----|\n\tText: ')

    # Pass text to the encrypt function
    encrypt(text)


    # Wait for Alice's reply
    print('\n\n|----Reply from Alice----|')

    response = client.recv(4096) # Buffer size to store message

    # Pass cypher text to decrypt function
    reply = decrypt(response)

    # Print message
    print("\n\tReply: ", reply)