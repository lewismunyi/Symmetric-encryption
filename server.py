# Lewis Munyi
# P15/36076/2015

# Alice

import socket
import threading



def decrypt(response):
    print( '\tCypher text {}'.format(response.decode("utf-8")))
    # print(request.decode("utf-8") )
    cyphertext = response.decode("utf-8")

    #decrypt the message
    mess = []
    for i in cyphertext:
        mess.append( chr( int( (ord(i)+20)/2 ) ))

    cypher = " ".join(mess)
    print( '\n\tText:  {}'.format(cypher))

def encrypt(message):
    val=[]
    for i in message:
        val.append(chr(int(ord(i)*2-20)))

    cyphertext = "".join(val)
    print("\tCyphertext: ", cyphertext, "\n\n")
    meso = str.encode(cyphertext)

    return meso
    

    

def handle_client_connection(client_socket):
    request = client_socket.recv(1024)
    print('|---- Message from Bob ----|')
    decrypt(request)

    
    print('\n\n|---- Reply to Bob ----|')
    prompt = input('\n\tText: ')
    message = str.encode(prompt)
    sen = message.decode("utf-8")
    mess = encrypt(sen)
    
    #send the message
    client_socket.send(mess)

    client_socket.close()

# Program start
if __name__ == "__main__":

    # Create Socket connection and start listening
    bind_ip = '0.0.0.0'
    bind_port = 3002
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind((bind_ip, bind_port))
    server.listen(5)  # max backlog of connections

    # print('Listening on {}:{}'.format(bind_ip, bind_port))
    print("Alice Online ...\n")

    while True:
        client_sock, address = server.accept()
        print( 'Connected to {}:{}'.format(address[0], address[1]))

        # Create a thread for each connection
        client_handler = threading.Thread(
            target=handle_client_connection,
            args=(client_sock,)  # without comma you'd get a "TypeError: handle_client_connection() argument after * must be a sequence, not _socketobject
        )
        client_handler.start()